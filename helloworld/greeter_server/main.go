/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

// Package main implements a server for Greeter service.
package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"

	pb "rpcdemo/helloworld/helloworld"
	"rpcdemo/internal/zero"

	"google.golang.org/grpc"
)

var (
	port = flag.Int("port", 50051, "The server port")
)

type server struct {
	pb.UnimplementedZeroServer
}

func (s *server) Get(in *pb.GetRequest, stream pb.Zero_GetServer) error {
	log.Printf("request: %+v", in)
	w := &writer{stream: stream}
	_, err := zero.Fetch(w, in.Total, int(in.Block))
	if err != nil {
		log.Printf("error: %w", err)
	}
	return err
}

type writer struct {
	resp   pb.GetReply
	stream pb.Zero_GetServer
}

func (w *writer) Write(p []byte) (int, error) {
	w.resp.Reset()
	w.resp.Data = p
	return len(p), w.stream.Send(&w.resp)
}

func main() {
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterZeroServer(s, &server{})
	log.Printf("server listening at %v", lis.Addr())

	lisPprof, err := net.Listen("tcp", fmt.Sprintf(":%d", *port+1))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("pprof listening at %v", lisPprof.Addr())
	go http.Serve(lisPprof, nil)

	if err := s.Serve(lis); err != nil {
		log.Fatal(err)
	}
}

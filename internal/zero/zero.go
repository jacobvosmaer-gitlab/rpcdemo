package zero

import (
	"fmt"
	"io"
	"os"
)

func Fetch(w io.Writer, total int64, block int) (int64, error) {
	if total < 0 {
		return 0, fmt.Errorf("invalid total: %d", total)
	}
	if block <= 0 {
		return 0, fmt.Errorf("invalid block: %d", total)
	}

	z, err := os.Open("/dev/zero")
	if err != nil {
		return 0, err
	}
	defer z.Close()

	buf := make([]byte, block)
	w = struct{ io.Writer }{w} // To level the playing field, block w.ReadFrom from being used
	return io.CopyBuffer(w, io.LimitReader(z, total), buf)
}

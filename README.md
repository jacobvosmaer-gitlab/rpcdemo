# rpcdemo

Demonstration of gRPC Go throughput limitations.

Recording of demo: [YouTube](https://youtu.be/MxGQ7WZ2N74).

Also see:

- [Blog post about Git fetch performance, part 1](https://about.gitlab.com/blog/2022/01/20/git-fetch-performance/)
- [Blog post about Git fetch performance, part 2](https://about.gitlab.com/blog/2022/02/07/git-fetch-performance-2021-part-2/)
- [Epic about transport efficiency](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/463)

## How to use

First build the executables.

```
./build
```

To test gRPC throughput first start the server:

```
./helloworld/greeter_server/greeter_server
```

Then run the client in a separate terminal:

```
time ./helloworld/greeter_client/greeter_client -total 5000000000
```

To compare with HTTP throughput, first start the server:

```
./httpzero/httpzero
```

Then use Curl as the client.

```
time curl -d 'total=5000000000&block=4096' -o /dev/null http://localhost:8080
```

On my machine, the difference in time is considerable. Note that not
only does the HTTP version require less wall clock time, it also uses
less total system and user time.

```
% time ./helloworld/greeter_client/greeter_client -total 5000000000 -block 32768
2023/04/21 08:46:51 received 5000000000 bytes
8.521u 11.616s 7.048r 	 ./helloworld/greeter_client/greeter_client -total 5000000000 -block 32768 ...
% time curl -d 'total=5000000000&block=32768' -o /dev/null http://localhost:8080
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
100  223M    0  223M  100    28  2052M    256 --:--:-- --:--:-- --:--:-- 2033M
100 2593M    0 2593M  100    28  2338M     25  0:00:01  0:00:01 --:--:-- 2336M
100 4768M    0 4768M  100    28  2374M     13  0:00:02  0:00:02 --:--:-- 2374M
0.714u 1.279s 2.017r 	 curl -d total=5000000000&block=32768 -o /dev/null ...
```

The wall time ("r" for "real") differs by a factor 3.5: 7.0 seconds with gRPC vs
2.0 seconds with Curl. Also, the gRPC client process runs at
286% CPU ((system + user) / real) while the Curl process runs at 99%
CPU. This is not even showing the difference in server CPU
utilization.

Consider these profiles captures with the `bin/pprof-record` script in this repository.

![greeter_server CPU profile](_assets/greeter_server_cpu.png)

The client took 7.3 seconds. The total server utilization of 13.9
seconds indicates the server process used 13.9/7.3 = 190% CPU.

![httpzero CPU profile](_assets/httpzero_cpu.png)

The client took 1.9 seconds. That means this server process used 100%
CPU to handle the request. Apart from that, the total CPU time spent
in the HTTP case is 7x less than in the gRPC case.

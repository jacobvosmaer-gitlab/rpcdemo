package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	_ "net/http/pprof"
	"strconv"

	"rpcdemo/internal/zero"
)

var port = flag.Int("port", 8080, "The server address")

func main() {
	flag.Parse()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if err := handle(w, r); err != nil {
			w.WriteHeader(500)
			log.Printf("error: %v", err)
		}
	})

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("server listening at %v", l.Addr())
	log.Fatal(http.Serve(l, nil))
}

func handle(w http.ResponseWriter, r *http.Request) error {
	if r.Method != "POST" {
		return fmt.Errorf("want POST got %q", r.Method)
	}

	total, err := strconv.ParseInt(r.FormValue("total"), 10, 64)
	if err != nil {
		return fmt.Errorf("parse total: %w", err)
	}
	block, err := strconv.Atoi(r.FormValue("block"))
	if err != nil {
		return fmt.Errorf("parse block: %w", err)
	}
	log.Printf("request: total: %d block: %d", total, block)

	w.Header().Set("Content-Type", "application/octet-stream")
	if _, err := zero.Fetch(w, total, block); err != nil {
		return err
	}

	return nil
}

type allocWriter struct{ w io.Writer }

func (aw *allocWriter) Write(p []byte) (int, error) {
	buf := make([]byte, len(p))
	copy(buf, p)
	return aw.w.Write(buf)
}

type writeResponse struct {
	n   int
	err error
}

type channelWriter struct {
	request  chan []byte
	response chan writeResponse
}

func newChannelWriter(ctx context.Context, w io.Writer) *channelWriter {
	cw := &channelWriter{
		request:  make(chan []byte),
		response: make(chan writeResponse),
	}
	go func() {
		for {
			select {
			case p := <-cw.request:
				n, err := w.Write(p)
				cw.response <- writeResponse{n, err}
			case <-ctx.Done():
				return
			}
		}
	}()

	return cw
}

func (cw *channelWriter) Write(p []byte) (int, error) {
	cw.request <- p
	response := <-cw.response
	return response.n, response.err
}
